val String.numVowels
    get() = count { "aieouy".contains(it)}

fun String.addEnthusiasm(amount: Int = 1) = this + "!".repeat(amount)

fun Any.easyPrint() = println(this)

fun <T> T.easyPrint2(): T {
    println(this)
    return this
}

infix fun String?.printWithDefault(default: String) = println(this ?: default)

fun main() {
    "Madrigal has left the building".addEnthusiasm().easyPrint()
    "Madrigal has left the building".easyPrint2().addEnthusiasm().easyPrint()
    42.easyPrint()
    "How many vowels".numVowels.easyPrint()

    val nullableString: String? = null
    nullableString printWithDefault "Default String"
}